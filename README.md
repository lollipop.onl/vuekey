# vuekey

`vuekey` is a library of typesafe data path for [`@Watch`](https://github.com/kaorun343/vue-property-decorator#Watch) and `vm.set`.

## Installation

```sh
$ npm i --save @lollipop-onl/vuekey
# or
$ yarn add @lollipop-onl/vuekey
```

## Example

### Simple Example

```ts
import { Component, Watch, Vue } from 'vue-property-decorator';
import { setupVuekey } from '@lollipop-onl/vuekey';

const vuekey = setupVuekey<SampleComponent>();

@Component
export default class SampleComponent extends Vue {
  counter = {
    count: 0,
    updatedAt: Date.now()
  };

  @Watch(vuekey('counter', 'count'))
  onUpdateCount(): void {
    this.$set(...vuekey(this.counter, 'updatedAt', Date.now()));
  }
}
```

### with vuex-typesafe-helper

Typesafely watch store data with [`@lollipop-onl/vuex-typesafe-helper`](https://www.npmjs.com/package/@lollipop-onl/vuex-typesafe-helper).

```ts
import { Component, Watch, Vue } from 'vue-property-decorator';
import { setupVuekey } from '@lollipop-onl/vuekey';
import { RootStore } from './types/vuex';

const vuekey = setupVuekey<SampleComponent>();

@Component
export default class SampleComponent extends Vue {
  $store!: RootStore;

  @Watch(vuekey('$store', 'state', 'profile', 'birthday'))
  onUpdateBirthday(): void {
    // do something
  }
}
```

## Notice !

* `vuekey` can verify up to **10** depths
* `watch` AAAA support only simple path.
  * OK: `foo.bar.baz`
  * NG: `foo[0]`, `foo['bar-baz']`

## License

MIT
