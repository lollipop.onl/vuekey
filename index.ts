/**
 * @file VuexのState, Getterを安全に参照させるためのユーティリティ
 */

import Vue from 'vue';

type ArrayType<T extends any[]> = T extends (infer V)[] ? V : never;

export interface Vuekey<V extends Record<any, any>> {
  <
    T1 extends Record<any, any> | undefined,
    T2 extends keyof Required<NonNullable<T1>>,
    T3 extends NonNullable<T1>[T2]
  >(
    t1: T1,
    t2: T2,
    t3: T3
  ): [T1, any, any];
  <
    T1 extends any[] | undefined,
    T2 extends number,
    T3 extends ArrayType<T1>
  >(
    t1: T1,
    t2: T2,
    t3: T3
  ): [T1, any, any];
  <
    T1 extends keyof Required<V>,
    T2 extends keyof Required<Required<V>[T1]>,
    T3 extends keyof Required<Required<Required<V>[T1]>[T2]>,
    T4 extends keyof Required<Required<Required<Required<V>[T1]>[T2]>[T3]>,
    T5 extends keyof Required<Required<Required<Required<Required<V>[T1]>[T2]>[T3]>[T4]>,
    T6 extends keyof Required<Required<Required<Required<Required<Required<V>[T1]>[T2]>[T3]>[T4]>[T5]>,
    T7 extends keyof Required<Required<Required<Required<Required<Required<Required<V>[T1]>[T2]>[T3]>[T4]>[T5]>[T6]>,
    T8 extends keyof Required<Required<Required<Required<Required<Required<Required<Required<V>[T1]>[T2]>[T3]>[T4]>[T5]>[T6]>[T7]>,
    T9 extends keyof Required<Required<Required<Required<Required<Required<Required<Required<Required<V>[T1]>[T2]>[T3]>[T4]>[T5]>[T6]>[T7]>[T8]>,
    T10 extends keyof Required<Required<Required<Required<Required<Required<Required<Required<Required<Required<V>[T1]>[T2]>[T3]>[T4]>[T5]>[T6]>[T7]>[T8]>[T9]>
  >(
    t1?: T1,
    t2?: T2,
    t3?: T3,
    t4?: T4,
    t5?: T5,
    t6?: T6,
    t7?: T7,
    t8?: T8,
    t9?: T9,
    t10?: T10,
    ...args: string[]
  ): string;
}

export const vuekey = (...args: any[]) => {
  const t1 = args[0];

  if (t1 && typeof t1 === 'object') {
    return args;
  }

  return args.filter((arg) => typeof arg === 'string').join('.');
}

export function setupVuekey<C extends Vue>() {
  return vuekey as Vuekey<C>;
}
